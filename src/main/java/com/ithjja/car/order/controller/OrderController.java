package com.ithjja.car.order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ithjja.car.common.model.QueryPage;
import com.ithjja.car.common.utils.WebUtils;
import com.ithjja.car.order.model.Order;
import com.ithjja.car.order.service.OrderService;

/**
 * 订单预约的web控制层
 * @author hejja
 */
@RestController
@RequestMapping("order")
public class OrderController {

    /** 订单的业务逻辑层接口 */
    @Autowired
    private OrderService orderService;

    /**
     * 添加订单
     * @param order 订单信息
     * @return 响应
     */
    @PutMapping("/add")
    public ResponseEntity<Order> addOrder(@RequestBody Order order) {
        order.setUid(WebUtils.getUserId());
        return ResponseEntity.ok(orderService.addOrder(order));
    }

    /**
     * 分页查询当前用户订单信息
     * @param params 分页条件
     * @return 订单信息
     */
    @GetMapping("/page/user")
    public ResponseEntity<PageInfo<Order>> pageUserOrder(QueryPage params) {
        return ResponseEntity.ok(orderService.page(params));
    }

    /**
     * 分页查询所有订单信息
     * @param params 分页条件
     * @return 订单信息
     */
    @GetMapping("/page/all")
    public ResponseEntity<PageInfo<Order>> pageAll(QueryPage params) {
        return ResponseEntity.ok(orderService.pageAll(params));
    }

    /**
     * 改变订单状态
     * @param oid 订单id
     * @param status 状态
     * @return 响应
     */
    @PutMapping("/updateStatus/{oid}/{status}")
    public ResponseEntity<?> updateStatus(@PathVariable("oid") Long oid, @PathVariable("status") Integer status) {
        orderService.updateStatus(oid, status);
        return ResponseEntity.noContent().build();
    }
}
