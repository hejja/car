package com.ithjja.car.order.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ithjja.car.order.model.Order;

/**
 * 订单预约的数据访问层
 * @author hejja
 */
@Mapper
public interface OrderMapper {

    /**
     * 根据用户id和产品id查询正在处理中的订单信息
     * @param uid 用户id
     * @param pid 产品id
     * @return 订单信息
     */
    @Select("select * from tbl_order where uid = #{uid} and pid = #{pid} and status = 1")
    @Results(id = "defaultOrderResultMapping", value = {
        @Result(column = "order_time", property = "orderTime"),
        @Result(column = "new_order_time", property = "newOrderTime")
    })
    Order findByUidAndPid(@Param("uid") Long uid, @Param("pid") Long pid);

    /**
     * 添加订单
     * @param order 订单信息
     */
    @Insert("insert into tbl_order values(#{id}, #{pid}, #{uid}, #{orderTime}, #{newOrderTime}, #{number}, #{price}, "
        + "#{total}, #{status})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn="id")
    void addOrder(Order order);

    /**
     * 根据订单id修改订单数量、总价、最新订单时间
     * @param id 订单id
     * @param number 数量
     * @param total 总价
     * @param newOrderTime 最新订单时间
     */
    @Update("update tbl_order set number = #{number}, total = #{total}, new_order_time = #{newOrderTime} "
        + "where id = #{id}")
    void updateNumber(@Param("id") Long id, @Param("number") Integer number, @Param("total") Double total,
        @Param("newOrderTime") Date newOrderTime);

    /**
     * 分页查询当前用户订单信息，级联产品信息
     * @param uid 用户id
     * @return 订单集合
     */
    @Select("select * from tbl_order where uid = #{uid}")
    @Results(id = "orderResultMappingProduct", value = {
        @Result(column = "order_time", property = "orderTime"),
        @Result(column = "new_order_time", property = "newOrderTime"),
        @Result(column = "pid", property = "menuProduct",
            one = @One(select = "com.ithjja.car.menu.mapper.MenuProductMapper.findProductById"))
    })
    List<Order> page(@Param("uid") Long uid);

    /**
     * 分页查询所有订单信息，级联产品信息
     * @return 订单集合
     */
    @Select("select * from tbl_order")
    @Results(id = "orderResultMappingProductAndUser", value = {
        @Result(column = "order_time", property = "orderTime"),
        @Result(column = "new_order_time", property = "newOrderTime"),
        @Result(column = "pid", property = "menuProduct",
            one = @One(select = "com.ithjja.car.menu.mapper.MenuProductMapper.findProductById")),
        @Result(column = "uid", property = "user",
            one = @One(select = "com.ithjja.car.user.mapper.UserMapper.findUserById"))
    })
    List<Order> pageAll();

    /**
     * 改变订单状态
     * @param id 订单id
     * @param status 状态
     */
    @Update("update tbl_order set status = #{status} where id = #{id}")
    void updateStatus(@Param("id") Long id, @Param("status") Integer status);

}
