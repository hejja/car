package com.ithjja.car.order.service;

import com.github.pagehelper.PageInfo;
import com.ithjja.car.common.model.QueryPage;
import com.ithjja.car.order.model.Order;

/**
 * 订单的业务逻辑层接口
 * @author hejja
 */
public interface OrderService {

    /**
     * 添加订单
     * @param order 订单信息
     * @return 创建后的订单信息
     */
    Order addOrder(Order order);

    /**
     * 分页查询当前订单信息
     * @param queryPage 分页条件
     * @return 分页数据
     */
    PageInfo<Order> page(QueryPage queryPage);

    /**
     * 分页查询所有订单信息
     * @param queryPage 分页条件
     * @return 分页数据
     */
    PageInfo<Order> pageAll(QueryPage queryPage);

    /**
     * 改变订单状态
     * @param oid 订单id
     * @param status 状态
     */
    void updateStatus(Long oid, Integer status);
}
