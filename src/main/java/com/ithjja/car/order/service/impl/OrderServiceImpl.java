package com.ithjja.car.order.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ithjja.car.common.model.QueryPage;
import com.ithjja.car.common.utils.CommUtils;
import com.ithjja.car.common.utils.WebUtils;
import com.ithjja.car.ex.CarException;
import com.ithjja.car.menu.service.MenuProductService;
import com.ithjja.car.order.consts.OrderConsts;
import com.ithjja.car.order.mapper.OrderMapper;
import com.ithjja.car.order.model.Order;
import com.ithjja.car.order.service.OrderService;

/**
 * 订单的业务逻辑层接口实现
 * @author hejja
 */
@Service
public class OrderServiceImpl implements OrderService {

    /** 订单预约的数据访问层 */
    @Autowired
    private OrderMapper orderMapper;

    /** 产品业务逻辑层接口 */
    @Autowired
    private MenuProductService menuProductService;

    @Override
    public Order addOrder(Order order) {
        Order orderInfo = orderMapper.findByUidAndPid(order.getUid(), order.getPid());
        Double price = menuProductService.findPriceById(order.getPid());
        if (orderInfo == null) {
            order.setOrderTime(new Date());
            order.setNewOrderTime(new Date());
            order.setStatus(OrderConsts.ORDER_STATUS_1);
            order.setPrice(price);
            order.setTotal(order.getNumber() * price);
            orderMapper.addOrder(order);
            return order;
        } else if (orderInfo.getNumber() != null && orderInfo.getNumber() > 0) {
            if (OrderConsts.ORDER_STATUS_1.equals(orderInfo.getStatus())) {
                Integer totalNumber = orderInfo.getNumber() + order.getNumber();
                orderInfo.setTotal(totalNumber * price);
                orderInfo.setNewOrderTime(new Date());
                orderMapper.updateNumber(orderInfo.getId(), totalNumber, orderInfo.getTotal(),
                    orderInfo.getNewOrderTime());
                return orderInfo;
            }
        }
        throw new CarException(CommUtils.getString("car.error"));
    }

    @Override
    public PageInfo<Order> page(QueryPage queryPage) {
        PageHelper.startPage(queryPage.getPage(), queryPage.getSize());
        return new PageInfo<>(orderMapper.page(WebUtils.getUserId()));
    }

    @Override
    public PageInfo<Order> pageAll(QueryPage queryPage) {
        PageHelper.startPage(queryPage.getPage(), queryPage.getSize());
        return new PageInfo<>(orderMapper.pageAll());
    }

    @Override
    public void updateStatus(Long oid, Integer status) {
        this.orderMapper.updateStatus(oid, status);
    }
}
