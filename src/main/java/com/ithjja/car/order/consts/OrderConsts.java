package com.ithjja.car.order.consts;

/**
 * 订单的常量类
 * @author hejja
 */
public class OrderConsts {

    /** 订单处理中 */
    public static final Integer ORDER_STATUS_1 = 1;

    /** 订单待付款 */
    public static final Integer ORDER_STATUS_2 = 2;

    /** 订单已处理 */
    public static final Integer ORDER_STATUS_3 = 3;

    /** 订单已取消 */
    public static final Integer ORDER_STATUS_4 = 4;

    /** 订单已驳回 */
    public static final Integer ORDER_STATUS_5 = 5;

}
