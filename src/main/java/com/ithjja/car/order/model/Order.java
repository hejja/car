package com.ithjja.car.order.model;

import java.io.Serializable;
import java.util.Date;

import com.ithjja.car.menu.model.MenuProduct;
import com.ithjja.car.user.model.User;

/**
 * 订单预约的实体类
 * @author hejja
 */
public class Order implements Serializable {

    /** 序列化id */
    private static final long serialVersionUID = 737991860266755876L;

    /** 自增id 主键 */
    private Long id;

    /** 产品id */
    private Long pid;

    /** 产品信息 */
    private MenuProduct menuProduct;

    /** 用户id */
    private Long uid;

    /** 用户信息 */
    private User user;

    /** 下单时间 */
    private Date orderTime;

    /** 最新下单时间 */
    private Date newOrderTime;

    /** 下单数量 */
    private Integer number;

    /** 单价 */
    private Double price;

    /** 下单总价 */
    private Double total;

    /**
     * 订单状态
     */
    private Integer status;

    // --------------------------------------------- Getter And Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public MenuProduct getMenuProduct() {
        return menuProduct;
    }

    public void setMenuProduct(MenuProduct menuProduct) {
        this.menuProduct = menuProduct;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Date getNewOrderTime() {
        return newOrderTime;
    }

    public void setNewOrderTime(Date newOrderTime) {
        this.newOrderTime = newOrderTime;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
