package com.ithjja.car.comment.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.ithjja.car.comment.model.Comment;

/**
 * 产品的业务层接口
 * @author hejja
 */
public interface CommentService {

    /**
     * 根据产品id查询评论信息
     * @param pid 产品id
     * @return 评论集合
     */
    List<Comment> findAllByPid(@Param("pid") Long pid);

    /**
     * 添加评论信息
     * @param comment 评论信息
     */
    void addComm(Comment comment);
}
