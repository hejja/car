package com.ithjja.car.comment.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ithjja.car.comment.mapper.CommentMapper;
import com.ithjja.car.comment.model.Comment;
import com.ithjja.car.comment.service.CommentService;
import com.ithjja.car.common.utils.CommUtils;
import com.ithjja.car.common.utils.WebUtils;
import com.ithjja.car.ex.CarException;

/**
 * 产品的业务层接口实现
 * @author hejja
 */
@Service
public class CommentServiceImpl implements CommentService {

    /** 评论的数据访问层 */
    @Autowired
    private CommentMapper commentMapper;

    @Override
    public List<Comment> findAllByPid(Long pid) {
        return commentMapper.findAllByPid(pid);
    }

    @Override
    public void addComm(Comment comment) {
        if (comment.getContent() == null || comment.getContent().length() <= 0) {
            throw new CarException(CommUtils.getString("comment.error.content.empty"));
        }
        if (comment.getContent().length() > 1000) {
            throw new CarException(CommUtils.getString("comment.error.content.maxLength"));
        }
        comment.setUid(WebUtils.getUserId());
        comment.setCreateTime(new Date());
        commentMapper.addComm(comment);
    }
}
