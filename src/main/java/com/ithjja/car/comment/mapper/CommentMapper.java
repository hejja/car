package com.ithjja.car.comment.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.ithjja.car.comment.model.Comment;

/**
 * 评论的数据访问层
 * @author hejja
 */
@Mapper
public interface CommentMapper {

    /**
     * 根据产品id查询评论信息
     * @param pid 产品id
     * @return 评论集合
     */
    @Select("select * from tbl_comment where pid = #{pid} order by create_time desc")
    @Results(id = "userMapping", value = {
        @Result(column = "create_time", property = "createTime"),
        @Result(column = "uid", property = "user",
            one = @One(select = "com.ithjja.car.user.mapper.UserMapper.findUserById"))
    })
    List<Comment> findAllByPid(@Param("pid") Long pid);

    /**
     * 添加评论信息
     * @param comment 评论信息
     */
    @Insert("insert into tbl_comment values(#{id}, #{pid}, #{uid}, #{content}, #{createTime})")
    @Results(id = "defaultCommentMapping", value = {
        @Result(column = "create_time", property = "createTime")
    })
    void addComm(Comment comment);
}
