package com.ithjja.car.comment.model;

import java.io.Serializable;
import java.util.Date;

import com.ithjja.car.user.model.User;

/**
 * 评论的实体类
 * @author hejja
 */
public class Comment implements Serializable {

    /** 序列化id */
    private static final long serialVersionUID = 8549926030714594674L;

    /** 评论id */
    private Long id;

    /** 产品id */
    private Long pid;

    /** 用户id */
    private Long uid;

    /** 用户对象 */
    private User user;

    /** 评论内容 */
    private String content;

    /** 评论创建时间 */
    private Date createTime;

    // --------------------------------------------------- Getter And Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
