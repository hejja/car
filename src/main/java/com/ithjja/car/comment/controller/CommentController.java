package com.ithjja.car.comment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ithjja.car.comment.model.Comment;
import com.ithjja.car.comment.service.CommentService;

/**
 * 评论的控制层
 * @author hejja
 */
@RestController
@RequestMapping("comment")
public class CommentController {

    /** 产品的业务层接口 */
    @Autowired
    private CommentService commentService;

    /**
     * 获取产品的评论信息
     * @param pid 产品id
     * @return 评论信息
     */
    @GetMapping("/getProductComm/{pid}")
    public ResponseEntity<List<Comment>> getComment(@PathVariable("pid") Long pid) {
        return ResponseEntity.ok(commentService.findAllByPid(pid));
    }

    /**
     * 添加评论信息
     * @param comment 评论信息
     * @return 响应
     */
    @PutMapping("/addComm")
    public ResponseEntity<?> addComm(@RequestBody Comment comment) {
        commentService.addComm(comment);
        return ResponseEntity.noContent().build();
    }
}
