package com.ithjja.car.home;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 主界面控制层, 做路由跳转
 * @author hejja
 */
@Controller
@RequestMapping("/home")
public class HomeController {

    /**
     * 返回登录界面
     * @return 登录界面
     */
    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    /**
     * 返回至注册界面
     * @return 注册界面
     */
    @RequestMapping("/register")
    public String toRegister() {
        return "register";
    }

    /**
     * 跳转至主界面
     * @return 主界面
     */
    @RequestMapping("/index")
    public String test(Principal principal) {
        System.out.println(principal.getName());
        return "index";
    }

}
