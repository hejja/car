package com.ithjja.car;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 主启动类
 * @author hejja
 */
@SpringBootApplication()
public class CarApplication {

    /**
     * 主启动方法
     * @param args 参数
     */
    public static void main(String[] args) {
        SpringApplication.run(CarApplication.class, args);
    }
}
