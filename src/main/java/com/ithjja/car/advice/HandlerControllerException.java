package com.ithjja.car.advice;

import org.springframework.expression.AccessException;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 自定义处理器
 * @author hejja
 */
//@RestControllerAdvice
public class HandlerControllerException {

    /**
     * 异常处理
     * @param e 异常
     * @return 异常信息
     */
    @ExceptionHandler(value = Exception.class)
    public String handlerException(Exception e) {
        if (e instanceof AccessException) {
            return "无权限";
        }
        return e.getMessage();
    }

}
