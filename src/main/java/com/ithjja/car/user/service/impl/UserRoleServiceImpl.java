package com.ithjja.car.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ithjja.car.user.model.UserRole;
import com.ithjja.car.user.mapper.UserRoleMapper;
import com.ithjja.car.user.service.UserRoleService;

/**
 * 用户角色业务逻辑层实现类
 * @author hejja
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {

    /** 用户角色数据访问层 */
    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public void addUserRole(UserRole userRole) {
        userRoleMapper.addUserRole(userRole);
    }
}
