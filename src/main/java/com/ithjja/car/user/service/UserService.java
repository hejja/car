package com.ithjja.car.user.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.github.pagehelper.PageInfo;
import com.ithjja.car.common.model.QueryPage;
import com.ithjja.car.user.model.User;
import com.ithjja.car.user.model.UserDTO;

/**
 * 用户逻辑层接口
 * @author hejja
 */
public interface UserService extends UserDetailsService {

    /**
     * 检测用户实现
     * @param user 用户信息
     */
    void checkRegisterInfo(UserDTO user);

    /**
     * 用户注册
     * @param user 用户信息
     */
    void register(User user);

    /**
     * 根据用户id查询用户信息
     * @param id 用户id
     * @return 用户信息
     */
    User findById(Long id);

    /**
     * 根据用户id修改用户密码
     * @param password 新密码
     * @param id 用户id
     */
    void updatePassword(String password, Long id);

    /**
     * 分有人查询所有未删除的用户信息
     * @param queryPage 分页条件
     * @return 用户集合
     */
    PageInfo<User> page(QueryPage queryPage);

    /**
     * 根据用户id改变用户状态
     * @param uid 用户id
     * @param status 用户状态
     */
    void updateStatusByUid(Long uid, Integer status);
}
