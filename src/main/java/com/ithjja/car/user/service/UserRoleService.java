package com.ithjja.car.user.service;

import com.ithjja.car.user.model.UserRole;

/**
 * 用户角色业务逻辑层接口
 * @author hejja
 */
public interface UserRoleService {

    /**
     * 添加用户角色
     * @param userRole 用户角色信息
     */
    void addUserRole(UserRole userRole);
}
