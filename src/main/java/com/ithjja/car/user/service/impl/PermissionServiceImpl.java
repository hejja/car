package com.ithjja.car.user.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ithjja.car.user.mapper.PermissionMapper;
import com.ithjja.car.user.model.Permission;
import com.ithjja.car.user.service.PermissionService;

/**
 * 权限的业务逻辑层接口实现
 * @author hejja
 */
@Service
public class PermissionServiceImpl implements PermissionService {

    /** 权限的数据访问层 */
    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public List<Permission> findAllPermission() {
        return permissionMapper.findAllPermission();
    }
}
