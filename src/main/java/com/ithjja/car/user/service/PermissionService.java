package com.ithjja.car.user.service;

import java.util.List;

import com.ithjja.car.user.model.Permission;

/**
 * 权限的业务逻辑层接口
 * @author hejja
 */
public interface PermissionService {

    /**
     * 查询所有权限
     * @return 权限列表
     */
    List<Permission> findAllPermission();
}
