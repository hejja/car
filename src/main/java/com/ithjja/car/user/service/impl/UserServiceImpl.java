package com.ithjja.car.user.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ithjja.car.common.model.QueryPage;
import com.ithjja.car.common.utils.CommUtils;
import com.ithjja.car.ex.CarException;
import com.ithjja.car.user.consts.RoleConsts;
import com.ithjja.car.user.consts.UserConsts;
import com.ithjja.car.user.model.Permission;
import com.ithjja.car.user.model.User;
import com.ithjja.car.user.model.UserDTO;
import com.ithjja.car.user.model.UserRole;
import com.ithjja.car.user.mapper.UserMapper;
import com.ithjja.car.user.service.UserRoleService;
import com.ithjja.car.user.service.UserService;

/**
 * 用户逻辑层接口实现
 * @author hejja
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    /** 用户数据访问层 */
    @Autowired
    private UserMapper userMapper;

    /** 用户角色业务逻辑层接口 */
    @Autowired
    private UserRoleService userRoleService;

    /** 加密算法 */
    @Autowired
    private BCryptPasswordEncoder encoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 查询用户是否存在
        User user = userMapper.findByUsername(username);
        if (user == null) {
            return null;
        }
        // 查询用户对应角色
        List<Permission> permissions = userMapper.findPermissionByUsername(username);
        List<GrantedAuthority> authorities = new ArrayList<>();
        permissions.forEach(permission -> {
            authorities.add(new SimpleGrantedAuthority(permission.getPermTag()));
        });
        user.setAuthorities(authorities);
        return user;
    }

    @Override
    public void checkRegisterInfo(UserDTO user) {
        if (StringUtils.isEmpty(user.getUsername()) || StringUtils.isEmpty(user.getName())
            || StringUtils.isEmpty(user.getPassword()) || StringUtils.isEmpty(user.getRePassword())) {
            // 检查用户信息
            throw new CarException(CommUtils.getString("user.error.information.incomplete"));
        } else if (user.getName().length() > com.ithjja.car.user.consts.UserConsts.USER_STANDARD_NAME_MAX_LENGTH) {
            // 用户姓名检查长度
            throw new CarException(CommUtils.getString("user.error.name.length"));
        } else if (user.getPassword().length() < com.ithjja.car.user.consts.UserConsts.USER_STANDARD_PASSWORD_MIN_LENGTH
                || user.getPassword().length() > com.ithjja.car.user.consts.UserConsts.USER_STANDARD_PASSWORD_MAX_LENGTH) {
            // 检验密码长度
            throw new CarException(CommUtils.getString("user.error.password.length"));
        } else if (!CommUtils.validateMobilePhone(user.getUsername())) {
            // 用户账户/手机号 是否合格
            throw new CarException(CommUtils.getString("user.error.phone.legitimate"));
        } else if (!user.getPassword().equals(user.getRePassword())) {
            // 检查两次密码是否一致
            throw new CarException(CommUtils.getString("user.error.password.atypism"));
        }
        // 判断用户账户是否存在
        Long count = userMapper.findUserCountByUsername(user.getUsername());
        if (count > 0) {
            throw new CarException(CommUtils.getString("user.error.username.exists"));
        }
    }

    @Override
    public void register(User user) {
        user.setCreateTime(new Date());
        user.setStatus(UserConsts.STATUS_NORMAL);
        user.setPassword(encoder.encode(user.getPassword()));
        userMapper.addUser(user);
        // 添加默认角色
        userRoleService.addUserRole(new UserRole(user.getId(), RoleConsts.ROLE_USER_ID));
    }

    @Override
    public User findById(Long id) {
        return userMapper.findUserById(id);
    }

    @Override
    public void updatePassword(String password, Long id) {
        if (password == null || password.length() < UserConsts.USER_STANDARD_PASSWORD_MIN_LENGTH
            || password.length() > UserConsts.USER_STANDARD_PASSWORD_MAX_LENGTH) {
            // 检验密码长度
            throw new CarException(CommUtils.getString("user.error.password.length"));
        }
        userMapper.updatePassword(encoder.encode(password), id);
    }

    @Override
    public PageInfo<User> page(QueryPage queryPage) {
        PageHelper.startPage(queryPage.getPage(), queryPage.getSize());
        return new PageInfo<>(userMapper.findUnDeletedAll());
    }

    @Override
    public void updateStatusByUid(Long uid, Integer status) {
        userMapper.updateStatusByUid(uid, status);
    }
}
