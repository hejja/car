package com.ithjja.car.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ithjja.car.common.model.QueryPage;
import com.ithjja.car.common.utils.WebUtils;
import com.ithjja.car.user.model.User;
import com.ithjja.car.user.model.UserDTO;
import com.ithjja.car.user.service.UserService;

/**
 * 用户控制层
 * @author hejja
 */
@RestController
@RequestMapping("/user")
public class UserController {

    /** 用户逻辑层接口 */
    @Autowired
    private UserService userService;

    /**
     * 用户注册
     * @param user 用户信息
     * @return 响应
     */
    @PostMapping("register")
    public ResponseEntity<String> register(@RequestBody UserDTO user) {
        // 判断用户信息
        userService.checkRegisterInfo(user);
        // 添加用户
        userService.register(user.convertUser());
        return ResponseEntity.ok().build();
    }

    /**
     * 查询当前登陆用户信息
     * @return 用户信息
     */
    @GetMapping("/info")
    public ResponseEntity<User> info() {
        return ResponseEntity.ok(userService.findById(WebUtils.getUserId()));
    }

    /**
     * 修改当前用户密码
     * @param password 新密码
     * @return 响应
     */
    @PostMapping("/updatePassword")
    public ResponseEntity<?> updatePassword(@RequestBody String password) {
        userService.updatePassword(password, WebUtils.getUserId());
        return ResponseEntity.noContent().build();
    }

    /**
     * 分页查询所有用户
     * @param params 分页条件
     * @return 用户数据
     */
    @GetMapping("/page/all")
    public ResponseEntity<PageInfo<User>> pageUser(QueryPage params) {
        return ResponseEntity.ok(userService.page(params));
    }

    /**
     * 改变用户冻结状态
     * @param uid 用户id
     * @param status 冻结状态
     * @return 响应
     */
    @PutMapping("/updateStatus/{uid}/{status}")
    public ResponseEntity<?> updateStatus(@PathVariable("uid") Long uid, @PathVariable("status") Integer status) {
        userService.updateStatusByUid(uid, status);
        return ResponseEntity.noContent().build();
    }
}
