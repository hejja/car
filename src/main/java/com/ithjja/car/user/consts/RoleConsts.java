package com.ithjja.car.user.consts;

/**
 * 角色常量类
 * @author hejja
 */
public class RoleConsts {

    /** 用户基本权限：默认所有用户都有 */
    public static final String ROLE_USER = "ROLE_USER";

    /** 用户基本权限id：默认所有用户都有 */
    public static final Long ROLE_USER_ID = 1L;
}
