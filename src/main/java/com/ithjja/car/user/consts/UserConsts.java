package com.ithjja.car.user.consts;

/**
 * 用户常量类
 * @author hejja
 */
public class UserConsts {

    /** 账号正常 */
    public static final Integer STATUS_NORMAL = 1;

    /** 账号冻结 */
    public static final Integer STATUS_FROZEN = 2;

    // ********************* 用户信息规范 *********************

    /** 用户名最大长度 */
    public static final int USER_STANDARD_NAME_MAX_LENGTH = 13;

    /** 用户密码最大长度 */
    public static final int USER_STANDARD_PASSWORD_MAX_LENGTH = 16;

    /** 用户密码最小长度 */
    public static final int USER_STANDARD_PASSWORD_MIN_LENGTH = 8;
}
