package com.ithjja.car.user.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ithjja.car.user.model.Permission;
import com.ithjja.car.user.model.User;

/**
 * 用户数据访问层
 * @author hejja
 */
@Mapper
public interface UserMapper {

    /**
     * 根据用户名查询用户信息
     * @param username 用户名
     * @return 用户信息
     */
    @Select("select * from tbl_user where username = #{username}")
    @Results(id = "defaultResultMapping", value = {
        @Result(id = true, property = "id", column = "id"),
        @Result(property = "username", column = "username"),
        @Result(property = "password", column = "password"),
        @Result(property = "name", column = "name"),
        @Result(property = "status", column = "status"),
        @Result(property = "deleted", column = "deleted"),
        @Result(property = "createTime", column = "create_time")
    })
    User findByUsername(@Param("username") String username);

    /**
     * 根据登陆账户查询用户属性与用户权限
     * @param username 登陆账号
     * @return 用户对象
     */
    @Select("select permission.* from tbl_user user inner join tbl_user_role user_role on user.id = user_role.uid "
        + " inner join tbl_role_permission role_permission on user_role.rid = role_permission.rid "
        + " inner join tbl_permission permission on role_permission.pid = permission.id "
        + " where user.username = #{username}")
    @Results({
        @Result(id = true, property = "id", column = "id"),
        @Result(property = "permName", column = "perm_name"),
        @Result(property = "permTag", column = "perm_tag"),
        @Result(property = "url", column = "url")
    })
    List<Permission> findPermissionByUsername(@Param("username") String username);

    /**
     * 通过用户账户查找用户数量
     * @param username 用户名
     * @return 用户数量
     */
    @Select("select count(*) from tbl_user where username=#{username}")
    Long findUserCountByUsername(@Param("username") String username);

    /**
     * 添加用户
     * @param user 用户信息
     */
    @Insert("insert into tbl_user values(#{id}, #{username},"
        + " #{password}, #{name}, #{status}, #{deleted}, #{createTime})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    void addUser(User user);

    /**
     * 根据用户id查询用户信息
     * @param id 用户id
     * @return 用户信息
     */
    @Select("select id, username, name, status, create_time from tbl_user where id = #{id}")
    @ResultMap("defaultResultMapping")
    User findUserById(@Param("id") Long id);

    /**
     * 根据用户id修改用户密码
     * @param password 新密码
     * @param id 用户id
     */
    @Update("update tbl_user set password = #{password} where id = #{id}")
    void updatePassword(@Param("password") String password, @Param("id") Long id);

    /**
     * 查询所有未删除的用户信息
     * @return 用户集合
     */
    @Select("select id, username, name, create_time, status from tbl_user where deleted = false")
    @ResultMap("defaultResultMapping")
    List<User> findUnDeletedAll();

    /**
     * 根据用户id改变用户状态
     * @param uid 用户id
     * @param status 用户状态
     */
    @Update("update tbl_user set status = #{status} where id = #{uid}")
    void updateStatusByUid(@Param("uid") Long uid, @Param("status") Integer status);
}
