package com.ithjja.car.user.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.ithjja.car.user.model.Role;

/**
 * 角色数据访问层
 * @author hejja
 */
@Mapper
public interface RoleMapper {

    /**
     * 根据用户id查询权限
     * @param uid 用户id
     * @return 权限集合
     */
    @Select("SELECT r.id, role_name, role_desc FROM tbl_role r, tbl_user_role ur "
        + " WHERE r.id = ur.rid and ur.uid = #{uid}")
    @Results({
        @Result(id = true, property = "id", column = "id"),
        @Result(property = "roleName", column = "role_name"),
        @Result(property = "roleDesc", column = "role_desc")
    })
    List<Role> findByUid(@Param("uid") Long uid);
}
