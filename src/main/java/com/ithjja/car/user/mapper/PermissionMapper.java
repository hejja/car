package com.ithjja.car.user.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.ithjja.car.user.model.Permission;

/**
 * 权限的数据访问层
 * @author hejja
 */
@Mapper
public interface PermissionMapper {

    /**
     * 查询所有权限
     * @return 权限列表
     */
    @Select(" select * from tbl_permission ")
    List<Permission> findAllPermission();
}
