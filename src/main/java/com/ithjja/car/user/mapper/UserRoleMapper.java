package com.ithjja.car.user.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

import com.ithjja.car.user.model.UserRole;

/**
 * 用户角色数据访问层
 * @author hejja
 */
@Mapper
public interface UserRoleMapper {

    /**
     * 添加用户角色
     * @param userRole 用户角色信息
     */
    @Insert("insert into tbl_user_role values(#{id}, #{uid}, #{rid})")
    void addUserRole(UserRole userRole);
}
