package com.ithjja.car.user.model;

import java.io.Serializable;

/**
 * 权限实体类
 * @author hejja
 */
public class Permission implements Serializable {

    /** 序列化id */
    private static final long serialVersionUID = -6795737910215329710L;

    /** 主键id 自增 */
    private Long id;

    /** 权限名称 */
    private String permName;

    /** 权限标识 */
    private String permTag;

    /** 请求url */
    private String url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPermName() {
        return permName;
    }

    public void setPermName(String permName) {
        this.permName = permName;
    }

    public String getPermTag() {
        return permTag;
    }

    public void setPermTag(String permTag) {
        this.permTag = permTag;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
