package com.ithjja.car.user.model;

import java.io.Serializable;

import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 角色实体
 * @author hejja
 */
public class Role implements GrantedAuthority, Serializable {

    /** 序列化id */
    private static final long serialVersionUID = 6116903587610391241L;

    /** 主键 自增 */
    private Long id;

    /** 角色名称 */
    private String roleName;

    /** 角色描述 */
    private String roleDesc;

    @JsonIgnore
    @Override
    public String getAuthority() {
        return this.roleName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }
}
