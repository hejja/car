package com.ithjja.car.user.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ithjja.car.user.consts.UserConsts;

/**
 * 用户实体
 * @author hejja
 */
public class User implements UserDetails, Serializable {

    /** 序列化id */
    private static final long serialVersionUID = 1229521651626514588L;

    /** 主键id 自增 */
    private Long id;

    /** 登陆账号/手机号 */
    private String username;

    /** 登陆密码 */
    private String password;

    /** 用户名称 */
    private String name;

    /** 创建时间 */
    private Date createTime;

    /**
     * 账号状态
     * 1：正常
     * 2：冻结
     */
    private Integer status;

    /**
     * 是否删除 默认为false
     * false：未删除
     * true：已删除
     */
    private Boolean deleted = Boolean.FALSE;

    /** 用户角色 */
    private List<Role> roles;

    /** 用户权限 */
    private List<GrantedAuthority> authorities = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public void setAuthorities(List<GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return UserConsts.STATUS_NORMAL.equals(this.status);
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return UserConsts.STATUS_NORMAL.equals(this.status);
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return UserConsts.STATUS_NORMAL.equals(this.status);
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return UserConsts.STATUS_NORMAL.equals(this.status);
    }
}
