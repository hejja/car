package com.ithjja.car.user.model;

import java.io.Serializable;

/**
 * 用户实体，用于接收请求过来的数据
 * @author hejja
 */
public class UserDTO implements Serializable {

    /** 序列化id */
    private static final long serialVersionUID = 8410161347028201985L;

    /** 主键 */
    private Long id;

    /** 登陆账号/手机号 */
    private String username;

    /** 登陆密码 */
    private String password;

    /** 重复密码，确认密码 */
    private String rePassword;

    /** 用户名称 */
    private String name;

    /**
     * 转换成用户Bean实体
     * @return 用户对象
     */
    public User convertUser() {
        User user = new User();
        user.setUsername(this.username);
        user.setPassword(this.password);
        user.setName(this.name);
        return user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRePassword() {
        return rePassword;
    }

    public void setRePassword(String rePassword) {
        this.rePassword = rePassword;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
