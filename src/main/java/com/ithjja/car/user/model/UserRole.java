package com.ithjja.car.user.model;

import java.io.Serializable;

/**
 * 用户角色实体
 * @author hejja
 */
public class UserRole implements Serializable {

    /** 序列化id */
    private static final long serialVersionUID = -2321694066650848687L;

    /** 主键id自增 */
    private Long id;

    /** 用户id */
    private Long uid;

    /** 角色id */
    private Long rid;

    /**
     * 无参构造
     */
    public UserRole() {

    }

    /**
     * 构造器
     * @param uid 用户id
     * @param rid 角色id
     */
    public UserRole(Long uid, Long rid) {
        this.uid = uid;
        this.rid = rid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getRid() {
        return rid;
    }

    public void setRid(Long rid) {
        this.rid = rid;
    }
}
