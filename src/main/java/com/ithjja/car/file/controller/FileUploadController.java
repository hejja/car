package com.ithjja.car.file.controller;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.ithjja.car.common.utils.CommUtils;
import com.ithjja.car.ex.CarException;

/**
 * 文件上传的web层控制器
 * @author hejja
 */
@RestController
@RequestMapping("upload")
public class FileUploadController {

    public static final String FILE_PATH = "E:\\img\\car\\product\\imgPath";

    /**
     * 上传产品文件
     * @param file 文件对象
     * @return 新的文件路径
     */
    @PostMapping("/product")
    public ResponseEntity<String> upload(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            throw new CarException(CommUtils.getString("file.upload.error.empty"));
        }
        File filePath = new File(FILE_PATH);
        if (!filePath.exists()) {
            filePath.mkdirs();
        }
        String newFileName = UUID.randomUUID() + file.getOriginalFilename();
        String productPath = FILE_PATH + File.separator + newFileName;
        File dest = new File(productPath);
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            throw new CarException(CommUtils.getString("file.upload.error.empty"));
        }
        return ResponseEntity.ok(newFileName);
    }
}
