package com.ithjja.car.ex;

/**
 * 异常处理
 * @author hejja
 */
public class CarException extends RuntimeException {

    /** 序列化id */
    private static final long serialVersionUID = -59503061042366472L;

    /**
     * 无参构造器
     */
    public CarException() {
        super();
    }

    /**
     * 构造器
     * @param e 异常信息
     */
    public CarException(String e) {
        super(e);
    }

    public CarException(String code, String message) {

    }

}
