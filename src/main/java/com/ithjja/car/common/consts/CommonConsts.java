package com.ithjja.car.common.consts;

/**
 * 公用常量类
 * @author hejja
 */
public class CommonConsts {

    /** 手机号正则 */
    public static final String MOBILE_PHONE_REGEX =
        "^(13[0-9]{9})|(15[0-9]{9})|(17[0-9]{9})|(18[0-9]{9})|(19[0-9]{9})$";
}
