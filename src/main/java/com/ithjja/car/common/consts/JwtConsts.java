package com.ithjja.car.common.consts;

/**
 * Jwt常量类
 * @author hejja
 */
public class JwtConsts {

    /** Jwt存放的角色信息key */
    public static final String ROLE_CLAIMS = "roles";

    /** Jwt存放的用户名key */
    public static final String CLAIM_USER_USERNAME = "username";

    /** Jwt存放的用户id的key */
    public static final String CLAIM_USER_ID = "id";

    /** 响应key：token */
    public static final String RESPONSE_KEY_TOKEN = "token";

    /** 响应key：user */
    public static final String RESPONSE_KEY_USER = "user";
}
