package com.ithjja.car.common.model;

import java.io.Serializable;

/**
 * 消息处理实体
 * @author hejja
 */
public class Chat implements Serializable {

    /** 序列化id */
    private static final long serialVersionUID = -3098683411419461724L;

    /** 发送目标 */
    private String to;

    /** 发送者 */
    private String from;

    /** 消息内容 */
    private String content;

    // ---------------------------------------------- Getter And Setter

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
