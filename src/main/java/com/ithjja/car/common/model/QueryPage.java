package com.ithjja.car.common.model;

import java.io.Serializable;

/**
 * 分页的简单包装类
 * @author hejja
 */
public class QueryPage implements Serializable {

    /** 序列化id */
    private static final long serialVersionUID = 5702853161764519578L;

    /** 当前页数 */
    private Integer page;

    /** 每页数量 */
    private Integer size;

    /** 排序 */
    private String orderable;

    // --------------------------------------------------- Getter And Setter


    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getOrderable() {
        return orderable;
    }

    public void setOrderable(String orderable) {
        this.orderable = orderable;
    }
}
