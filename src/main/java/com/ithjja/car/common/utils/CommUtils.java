package com.ithjja.car.common.utils;

import java.util.regex.Pattern;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import com.ithjja.car.common.consts.CommonConsts;

/**
 * 公用工具类
 * @author hejja
 */
public class CommUtils {

    /**
     * 正则：手机号
     * @param phone 手机号
     * @return 是否合格
     */
    public static boolean validateMobilePhone(String phone) {
        Pattern pattern = Pattern.compile(CommonConsts.MOBILE_PHONE_REGEX);
        return pattern.matcher(phone).matches();
    }

    /**
     * 根据消息键和参数 获取消息 委托给spring messageSource
     *
     * @param code 消息键
     * @param args 参数
     * @return 获取国际化翻译值
     */
    public static String getString(String code, Object... args) {
        MessageSource messageSource = SpringUtils.getBean(MessageSource.class);
        return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());
    }

}
