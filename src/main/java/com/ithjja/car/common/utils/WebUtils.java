package com.ithjja.car.common.utils;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * web相关的工具类
 * @author hejja
 */
public class WebUtils extends org.springframework.web.util.WebUtils {

    /**
     * 获取当前请求对象
     * @return 请求对象
     */
    @SuppressWarnings("all")
    public static HttpServletRequest getHttpServletRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

    /**
     * 获取token
     * @return token
     */
    public static String getToken() {
        return getHttpServletRequest().getHeader("token");
    }

    /**
     * 获取当前登陆用户id
     * @return 当前登陆用户id
     */
    public static Long getUserId() {
        return JwtUtils.getUserIdByToken(getToken());
    }
}
