package com.ithjja.car.common.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.List;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.alibaba.fastjson.JSONArray;
import com.ithjja.car.common.consts.JwtConsts;
import com.ithjja.car.user.model.User;

/**
 * Jwt工具类
 * @author hejja
 */
public class JwtUtils {

//    @Value("${jwt.subject}")
    private static String subject = "car";

//    @Value("${jwt.tokenSeconds}")
    private static Long tokenSeconds = 14400000L;

//    @Value("${jwt.base64Secret}")
    private static String base64Secret = "car_secret";


    /**
     * 生成用户Token
     * @param user 用户信息
     * @return Token
     */
    public static String generateJsonWebToken(User user) {
        return Jwts
            .builder()
            .setSubject(subject)
            .claim(JwtConsts.ROLE_CLAIMS, user.getAuthorities())
            .claim(JwtConsts.CLAIM_USER_USERNAME, user.getUsername())
            .claim(JwtConsts.CLAIM_USER_ID, user.getId())
            .setIssuedAt(new Date())
            .setExpiration(new Date(System.currentTimeMillis() + tokenSeconds))
            .signWith(SignatureAlgorithm.HS256, base64Secret).compact();
    }

    /**
     * 获取用户名
     * @param token Token
     * @return 用户名
     */
    public static String getUsernameByToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(base64Secret).parseClaimsJws(token).getBody();
        return claims.get(JwtConsts.CLAIM_USER_USERNAME).toString();
    }

    /**
     * 获取用户名
     * @param token Token
     * @return 用户名
     */
    public static Long getUserIdByToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(base64Secret).parseClaimsJws(token).getBody();
        return Long.valueOf(claims.get(JwtConsts.CLAIM_USER_ID).toString());
    }

    /**
     * 获取用户角色
     * @param token Token
     * @return 用户角色
     */
    @SuppressWarnings("all")
    public static List<SimpleGrantedAuthority> getUserRole(String token) {
        Claims claims = Jwts.parser().setSigningKey(base64Secret).parseClaimsJws(token).getBody();
        List roles = (List) claims.get(JwtConsts.ROLE_CLAIMS);
        String json = JSONArray.toJSONString(roles);
        return JSONArray.parseArray(json, SimpleGrantedAuthority.class);
    }

}
