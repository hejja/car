package com.ithjja.car.config;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.ithjja.car.filter.JwtAuthorizationFilter;
import com.ithjja.car.filter.JwtLoginFilter;
import com.ithjja.car.user.model.Permission;
import com.ithjja.car.user.service.PermissionService;
import com.ithjja.car.user.service.UserService;

/**
 * Security配置类
 * @author hejja
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    /** 用户逻辑层接口 */
    @Autowired
    private UserService userService;

    /** 权限的业务逻辑层接口 */
    @Autowired
    private PermissionService permissionService;

    public static void main(String[] args) {
        System.out.println(new BCryptPasswordEncoder().encode("admin"));
    }

    /**
     * 加密方式
     * @return 加密方式
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        List<Permission> permissions = permissionService.findAllPermission();
        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry registry
            = http.authorizeRequests();
        permissions.forEach(permission -> {
            registry.antMatchers(permission.getUrl()).hasAnyAuthority(permission.getPermTag());
        });
        registry.antMatchers("/auth/login").permitAll()
            .antMatchers("/**").fullyAuthenticated()
            .and()
            .addFilter(new JwtAuthorizationFilter(authenticationManager()))
            .addFilter(new JwtLoginFilter(authenticationManager())).csrf().disable()
            // 不需要session
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/chat/**", "/user/register");
    }
}
