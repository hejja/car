package com.ithjja.car.config;

import java.security.Principal;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import com.ithjja.car.common.utils.JwtUtils;

/**
 * WebSocket配置类
 * @author hejja
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        // 设置消息代理前缀
        config.enableSimpleBroker("/topic", "/queue");
        // 设置被@MessageMaping处理消息的前缀
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/chat").setAllowedOrigins("*").withSockJS();
    }

    /**
     * 自定义的Principal
     */
    class WebsocketUserVO implements Principal {
        private  String id;
        public WebsocketUserVO(String id) {
            this.id = id;
        }

        @Override
        public String getName() {
            return id;
        }
    }

    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        registration.setInterceptors(new ChannelInterceptorAdapter() {
            @Override
            public Message<?> preSend(Message<?> message, MessageChannel channel) {
                StompHeaderAccessor accessor =
                        MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
                if (StompCommand.CONNECT.equals(accessor.getCommand())) {
                    String jwtToken = accessor.getFirstNativeHeader("token");
                    if (StringUtils.isNotEmpty(jwtToken)) {
                        Object raw = message.getHeaders().get(SimpMessageHeaderAccessor.NATIVE_HEADERS);
                        if (raw instanceof Map) {
                            accessor.setUser(new WebsocketUserVO(JwtUtils.getUserIdByToken(jwtToken).toString()));
                        }
                    }
                }
                return message;
            }
        });
    }
}
