package com.ithjja.car.config;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.ithjja.car.ex.CarException;

/**
 * 异常处理配置
 * @author hejja
 */
@RestControllerAdvice()
public class CarExceptionHandler {

    /**
     * CarException异常处理
     * @param e 异常
     * @return 异常信息
     */
    @ExceptionHandler(CarException.class)
    @ResponseBody
    public ResponseEntity<?> carException(CarException e) {
        return ResponseEntity.ok(e.getMessage());
    }
}
