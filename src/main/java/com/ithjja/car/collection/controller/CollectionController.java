package com.ithjja.car.collection.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ithjja.car.collection.model.Collection;
import com.ithjja.car.collection.service.CollectionService;
import com.ithjja.car.common.model.QueryPage;
import com.ithjja.car.common.utils.WebUtils;

/**
 * 收藏的web控制层
 * @author hejja
 */
@RestController
@RequestMapping("collection")
public class CollectionController {

    /** 收藏的业务逻辑层接口 */
    @Autowired
    private CollectionService collectionService;

    /**
     * 查询是否收藏过产品
     * @param pid 产品id
     * @return 响应
     */
    @GetMapping("/whetherCollection/{pid}")
    public ResponseEntity<Boolean> whetherCollectionProduct(@PathVariable("pid") Long pid) {
        return ResponseEntity.ok(collectionService.whetherCollectionProduct(WebUtils.getUserId(), pid));
    }

    /**
     * 收藏产品
     * @param pid 产品id
     * @return 响应
     */
    @PutMapping("/addCollection/{pid}")
    public ResponseEntity<?> addCollection(@PathVariable("pid") Long pid) {
        collectionService.addCollection(WebUtils.getUserId(), pid);
        return ResponseEntity.noContent().build();
    }

    /**
     * 分页查询收藏信息
     * @param params 分页条件
     * @return 收藏信息
     */
    @GetMapping("/page/all")
    public ResponseEntity<PageInfo<Collection>> pageCollection(QueryPage params) {
        return ResponseEntity.ok(collectionService.page(params));
    }

    /**
     * 删除收藏
     * @param id 收藏id
     * @return 响应
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        collectionService.delete(id);
        return ResponseEntity.noContent().build();
    }

}
