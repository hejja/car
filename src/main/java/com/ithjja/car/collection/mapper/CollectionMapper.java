package com.ithjja.car.collection.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ithjja.car.collection.model.Collection;

/**
 * 收藏的数据访问层
 * @author hejja
 */
@Mapper
public interface CollectionMapper {

    /**
     * 根据用户id和产品id查询收藏信息
     * @param uid 用户id
     * @param pid 产品id
     * @return 收藏信息
     */
    @Select("select * from tbl_collection where uid = #{uid} and pid = #{pid}")
    @Results(id = "defaultResultMapping", value = {
        @Result(property = "createTime", column = "create_time")
    })
    Collection findCountByUidAndPid(@Param("uid") Long uid, @Param("pid") Long pid);

    /**
     * 根据收藏id修改删除状态
     * @param deleted 删除状态
     * @param id 收藏id
     */
    @Update("update tbl_collection set deleted = #{deleted} where id = #{id}")
    void updateDeleted(@Param("deleted") Boolean deleted, @Param("id") Long id);

    /**
     * 添加收藏
     * @param collection 收藏信息
     */
    @Insert("insert into tbl_collection values(#{id}, #{uid}, #{pid}, #{createTime}, #{deleted})")
    void addCollection(Collection collection);

    /**
     * 分页查询当前用户收藏信息，级联产品信息
     * @param uid 用户id
     * @return 收藏集合
     */
    @Select("select * from tbl_collection where uid = #{uid} and deleted = false")
    @Results(id = "collectionResultMappingProduct", value = {
        @Result(property = "createTime", column = "create_time"),
        @Result(column = "pid", property = "menuProduct",
            one = @One(select = "com.ithjja.car.menu.mapper.MenuProductMapper.findProductById"))
    })
    List<Collection> page(@Param("uid") Long uid);

}
