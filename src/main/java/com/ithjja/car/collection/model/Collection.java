package com.ithjja.car.collection.model;

import java.io.Serializable;
import java.util.Date;

import com.ithjja.car.menu.model.MenuProduct;
import com.ithjja.car.user.model.User;

/**
 * 收藏的实体类
 * @author hejja
 */
public class Collection implements Serializable {

    /** 序列化id */
    private static final long serialVersionUID = 5320597902506513418L;

    /** 主键id 自增 */
    private Long id;

    /** 用户id */
    private Long uid;

    /** 用户信息 */
    private User user;

    /** 产品id */
    private Long pid;

    /** 产品信息 */
    private MenuProduct menuProduct;

    /** 创建时间 */
    private Date createTime;

    /**
     * 是否删除，默认为false
     * true：已删除
     * false：未删除
     */
    private Boolean deleted = Boolean.FALSE;

    // --------------------------------------------------------------- Getter And Setter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public MenuProduct getMenuProduct() {
        return menuProduct;
    }

    public void setMenuProduct(MenuProduct menuProduct) {
        this.menuProduct = menuProduct;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
