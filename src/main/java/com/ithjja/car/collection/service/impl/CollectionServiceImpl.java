package com.ithjja.car.collection.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ithjja.car.collection.mapper.CollectionMapper;
import com.ithjja.car.collection.model.Collection;
import com.ithjja.car.collection.service.CollectionService;
import com.ithjja.car.common.model.QueryPage;
import com.ithjja.car.common.utils.WebUtils;

/**
 * 收藏的业务逻辑层接口实现
 * @author hejja
 */
@Service
public class CollectionServiceImpl implements CollectionService {

    /** 收藏的数据访问层 */
    @Autowired
    private CollectionMapper collectionMapper;

    @Override
    public Boolean whetherCollectionProduct(Long uid, Long pid) {
        Collection collection = collectionMapper.findCountByUidAndPid(uid, pid);
        return collection == null || collection.getDeleted();
    }

    @Override
    public void addCollection(Long uid, Long pid) {
        Collection collection = collectionMapper.findCountByUidAndPid(uid, pid);
        if (collection == null) {
            collection = new Collection();
            collection.setUid(uid);
            collection.setPid(pid);
            collection.setCreateTime(new Date());
            collectionMapper.addCollection(collection);
        } else if (collection.getDeleted()) {
            collectionMapper.updateDeleted(Boolean.FALSE, collection.getId());
        } else {
            throw new RuntimeException();
        }
    }

    @Override
    public PageInfo<Collection> page(QueryPage queryPage) {
        PageHelper.startPage(queryPage.getPage(), queryPage.getSize());
        return new PageInfo<>(collectionMapper.page(WebUtils.getUserId()));
    }

    @Override
    public void delete(Long id) {
        collectionMapper.updateDeleted(Boolean.TRUE, id);
    }
}
