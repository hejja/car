package com.ithjja.car.collection.service;

import com.github.pagehelper.PageInfo;
import com.ithjja.car.collection.model.Collection;
import com.ithjja.car.common.model.QueryPage;

/**
 * 收藏的业务逻辑层接口
 * @author hejja
 */
public interface CollectionService {

    /**
     * 查询是否收藏产品
     * @param uid 用户id
     * @param pid 产品id
     * @return 是否收藏
     */
    Boolean whetherCollectionProduct(Long uid, Long pid);

    /**
     * 收藏产品
     * @param uid 用户id
     * @param pid 产品id
     */
    void addCollection(Long uid, Long pid);

    /**
     * 分页查询未删除的收藏信息
     * @param queryPage 分页条件
     * @return 分页数据
     */
    PageInfo<Collection> page(QueryPage queryPage);

    /**
     * 根据收藏id删除收藏
     * @param id 收藏id
     */
    void delete(Long id);

}
