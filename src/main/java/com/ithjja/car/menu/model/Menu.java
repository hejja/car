package com.ithjja.car.menu.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 菜单实体
 * @author hejja
 */
public class Menu implements Serializable {

    /** 序列化id */
    private static final long serialVersionUID = 4697898356131000336L;

    /** 主键id 自增 */
    private Long id;

    /** 菜单名 */
    private String name;

    /** 展示排序 */
    private Long sort;

    /** 创建时间 */
    private Date createTime;

    /** 菜单描述信息 */
    private String describeInfo;

    /**
     * 是否删除，默认为false
     * true：已删除
     * false：未删除
     */
    private Boolean deleted = Boolean.FALSE;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getDescribeInfo() {
        return describeInfo;
    }

    public void setDescribeInfo(String describeInfo) {
        this.describeInfo = describeInfo;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
