package com.ithjja.car.menu.model;

import java.io.Serializable;
import java.util.List;

/**
 * 前台展示实体
 * @author hejja
 */
public class FrontMenu implements Serializable {

    /** 序列化id */
    private static final long serialVersionUID = 8353899417487042523L;

    /** 菜单 */
    private List<Menu> menus;

    /** 产品 */
    private List<MenuProduct> products;

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    public List<MenuProduct> getProducts() {
        return products;
    }

    public void setProducts(List<MenuProduct> products) {
        this.products = products;
    }
}
