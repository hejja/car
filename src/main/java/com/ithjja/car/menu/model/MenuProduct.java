package com.ithjja.car.menu.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 产品实体
 * @author hejja
 */
public class MenuProduct implements Serializable {

    /** 序列化id */
    private static final long serialVersionUID = -2740027171615927568L;

    /** 主键id 自增 */
    private Long id;

    /** 菜单id */
    private Long mid;

    /** 菜单信息 */
    private Menu menu;

    /** 产品名 */
    private String name;

    /** 产品图面路径 */
    private String imgPath;

    /** 产品价格 */
    private Double price;

    /** 菜单描述信息 */
    private String describeInfo;

    /** 创建时间 */
    private Date createTime;

    /** 是否删除 */
    private Boolean deleted = Boolean.FALSE;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMid() {
        return mid;
    }

    public void setMid(Long mid) {
        this.mid = mid;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getDescribeInfo() {
        return describeInfo;
    }

    public void setDescribeInfo(String describeInfo) {
        this.describeInfo = describeInfo;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
