package com.ithjja.car.menu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.ithjja.car.menu.model.Menu;

/**
 * 菜单数据访问层
 * @author hejja
 */
@Mapper
public interface MenuMapper {

    /**
     * 查询所有未删除的菜单
     * @return 菜单集合
     */
    @Select("select * from tbl_menu where deleted = false order by show_sort asc")
    @Results({
        @Result(property = "createTime", column = "create_time"),
        @Result(property = "describeInfo", column = "describe_info"),
        @Result(property = "sort", column = "show_sort")
    })
    List<Menu> findAllUnDeleted();

    /**
     * 添加菜单
     * @param menu 菜单信息
     */
    @Insert("insert into tbl_menu values(#{id}, #{name}, #{sort}, " +
        "#{createTime}, #{describeInfo}, #{deleted})")
    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    void addMenu(Menu menu);

    /**
     * 根据id删除菜单
     * @param id 菜单id
     */
    @Update("update tbl_menu set deleted = true where id = #{id}")
    void deletedById(@Param("id") Long id);

    /**
     * 根据id，修改菜单删除状态
     * @param id 菜单id
     * @param deleted 删除状态
     */
    @Update("update tbl_menu set deleted = #{deleted} where id = #{id}")
    void updateDeleted(@Param("id") Long id, @Param("deleted") Boolean deleted);

    /**
     * 根据id修改菜单信息
     * @param menu 菜单信息
     */
    @Update("update tbl_menu set name = #{name}, describe_info = #{describeInfo},"
        + " where id = #{id}")
    void updateMenuById(Menu menu);

    /**
     * 根据菜单名查询菜单数量
     * @param name 菜单名
     * @return 菜单数量
     */
    @Select("select count(*) from tbl_menu where name = #{name}")
    Long findCountByName(@Param("name") String name);

    /**
     * 根据菜单id查询菜单信息
     * @param id 菜单id
     * @return 菜单信息
     */
    @Select("select * from tbl_menu where id = #{id}")
    Menu findById(@Param("id") Long id);
}
