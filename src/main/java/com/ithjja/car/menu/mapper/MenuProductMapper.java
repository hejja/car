package com.ithjja.car.menu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.ithjja.car.menu.model.MenuProduct;

/**
 * 产品数据访问层
 * @author hejja
 */
@Mapper
public interface MenuProductMapper {

    /**
     * 查询所有未删除的产品信息
     * @return 产品信息裂变
     */
    @Select("select * from tbl_menu_product where deleted = false")
    @Results(id = "defaultResultMapping", value = {
        @Result(property = "imgPath", column = "img_path"),
        @Result(property = "describeInfo", column = "describe_info"),
        @Result(property = "createTime", column = "create_time")
    })
    List<MenuProduct> findAllUnDeleted();

    /**
     * 根据菜单id查询产品
     * @param mid 菜单id
     * @return 产品集合
     */
    @Select("select * from tbl_menu_product where mid = #{mid} and deleted = false")
    @ResultMap("defaultResultMapping")
    List<MenuProduct> findByMidUnDeleted(@Param("mid") Long mid);

    /**
     * 根据产品创建时间倒叙，分页查询产品信息
     * @param number 查询多少页
     * @return 产品集合
     */
    @Select("select * from tbl_menu_product order by create_time desc limit 0, #{number}")
    @ResultMap("defaultResultMapping")
    List<MenuProduct> findLimit(@Param("number") Integer number);

    /**
     * 根据产品id查询产品，级联查询所属菜单信息
     * @param id 产品id
     * @return 产品信息
     */
    @Select("select * from tbl_menu_product where id = #{id}")
    @Results(id = "menuResultMapping",value = {
        @Result(property = "imgPath", column = "img_path"),
        @Result(property = "describeInfo", column = "describe_info"),
        @Result(property = "createTime", column = "create_time"),
        @Result(column = "mid", property = "menu",
            one = @One(select = "com.ithjja.car.menu.mapper.MenuMapper.findById"))
    })
    MenuProduct findById(@Param("id") Long id);

    /**
     * 根据产品id查询产品
     * @param id 产品id
     * @return 产品信息
     */
    @Select("select * from tbl_menu_product where id = #{id}")
    @ResultMap("defaultResultMapping")
    MenuProduct findProductById(@Param("id") Long id);

    /**
     * 根据产品id查询价格
     * @param id 产品id
     * @return 价格
     */
    @Select("select price from tbl_menu_product where id = #{id}")
    Double findPriceById(@Param("id") Long id);

    /**
     * 根据菜单id查询产品数量
     * @param mid 菜单id
     * @return 数量
     */
    @Select("select count(*) from tbl_menu_product where mid = #{mid}")
    Long findCountByMid(@Param("mid") Long mid);

    /**
     * 根据菜单id分页查询产品
     * @param mid 菜单id
     * @return 产品集合
     */
    @Select("select * from tbl_menu_product where mid = #{mid} and deleted = false")
    @ResultMap("defaultResultMapping")
    List<MenuProduct> page(@Param("mid") Long mid);

    /**
     * 分页查询产品
     * @return 产品集合
     */
    @Select("select * from tbl_menu_product")
    @ResultMap("menuResultMapping")
    List<MenuProduct> pageAll();

    @Insert("insert into tbl_menu_product values(#{id}, #{mid}, #{name}, #{imgPath}, #{price}, "
        + "#{describeInfo}, #{createTime}, #{deleted})")
    void add(MenuProduct menuProduct);
}
