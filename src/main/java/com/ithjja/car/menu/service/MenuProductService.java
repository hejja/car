package com.ithjja.car.menu.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.ithjja.car.common.model.QueryPage;
import com.ithjja.car.menu.model.MenuProduct;

/**
 * 产品业务逻辑层接口
 * @author hejja
 */
public interface MenuProductService {

    /**
     * 根据产品创建时间倒叙，分页查询产品信息
     * @param number 查询多少页
     * @return 产品集合
     */
    List<MenuProduct> findLimit(Integer number);

    /**
     * 根据产品id查询产品
     * @param id 产品id
     * @return 产品信息
     */
    MenuProduct findById(Long id);

    /**
     * 根据产品id查询价格
     * @param id 产品id
     * @return 价格
     */
    Double findPriceById(Long id);

    /**
     * 根据菜单id查询产品数量
     * @param mid 菜单id
     * @return 数量
     */
    Long findCountByMind(Long mid);

    /**
     * 根据菜单id分页查询产品
     * @param mid 菜单id
     * @param queryPage 分页条件
     * @return 产品集合
     */
    PageInfo<MenuProduct> page(Long mid, QueryPage queryPage);

    /**
     * 分页查询产品
     * @param queryPage 分页条件
     * @return 产品集合
     */
    PageInfo<MenuProduct> pageAll(QueryPage queryPage);

    List<MenuProduct> findByMid(Long mid);

    void add(MenuProduct menuProduct);
}
