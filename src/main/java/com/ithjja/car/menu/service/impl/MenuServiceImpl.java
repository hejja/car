package com.ithjja.car.menu.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ithjja.car.common.utils.CommUtils;
import com.ithjja.car.ex.CarException;
import com.ithjja.car.menu.mapper.MenuMapper;
import com.ithjja.car.menu.model.FrontMenu;
import com.ithjja.car.menu.model.Menu;
import com.ithjja.car.menu.model.MenuProduct;
import com.ithjja.car.menu.service.MenuProductService;
import com.ithjja.car.menu.service.MenuService;
import com.ithjja.car.setting.consts.SettingConsts;
import com.ithjja.car.setting.model.SettingProduct;
import com.ithjja.car.setting.service.SettingProductService;

/**
 * 菜单的业务逻辑层实现了
 * @author hejja
 */
@Service
public class MenuServiceImpl implements MenuService {

    /** 菜单数据访问层 */
    @Autowired
    private MenuMapper menuMapper;

    /** 产品配置业务逻辑层接口 */
    @Autowired
    private SettingProductService settingProductService;

    /** 产品业务逻辑层接口 */
    @Autowired
    private MenuProductService menuProductService;

    @Override
    public FrontMenu frontDisplay() {
        FrontMenu frontMenu = new FrontMenu();
        // 所有未删除的菜单
        List<Menu> menus = findAllUnDeleted();
        // 查询产品配置
        SettingProduct settingProduct =
            settingProductService.findByName(SettingConsts.SETTING_PRODUCT_NAME_FRONT_DISPLAY);
        Integer itemNumber = Integer.valueOf(settingProduct.getItem());
        List<MenuProduct> products = menuProductService.findLimit(itemNumber);
        frontMenu.setMenus(menus);
        frontMenu.setProducts(products);
        return frontMenu;
    }

    @Override
    public List<Menu> findAllUnDeleted() {
        return menuMapper.findAllUnDeleted();
    }

    @Override
    public PageInfo<Menu> page(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        return new PageInfo<>(findAllUnDeleted());
    }

    @Override
    public void addMenu(Menu menu) {
        if (findCountByName(menu.getName()) > 0) {
            menuMapper.updateDeleted(menu.getId(), Boolean.FALSE);
            return;
        }
        menuMapper.addMenu(menu);
    }

    @Override
    public void deletedById(Long id) {
        if (menuProductService.findCountByMind(id) > 0) {
            throw new CarException(CommUtils.getString("menu.error.exists.product"));
        }
        menuMapper.deletedById(id);
    }

    @Override
    public void updateMenuById(Menu menu) {
        if (menu == null || StringUtils.isEmpty(menu.getName())) {
            throw new CarException(CommUtils.getString("menu.error.name.isEmpty"));
        }
        if (findCountByName(menu.getName()) > 0) {
            throw new CarException(CommUtils.getString("menu.error.name.exists"));
        }
        menuMapper.updateMenuById(menu);
    }

    @Override
    public Long findCountByName(String name) {
        return menuMapper.findCountByName(name);
    }

}
