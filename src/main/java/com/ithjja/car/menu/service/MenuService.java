package com.ithjja.car.menu.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.ithjja.car.menu.model.FrontMenu;
import com.ithjja.car.menu.model.Menu;

/**
 * 菜单的业务逻辑层接口
 * @author hejja
 */
public interface MenuService {

    /**
     * 前台展示数据
     * @return 前台数据
     */
    FrontMenu frontDisplay();

    /**
     * 查询所有未删除的菜单
     * @return 菜单集合
     */
    List<Menu> findAllUnDeleted();

    /**
     * 分页查询所有未删除的菜单
     * @param pageNum 第几页
     * @param pageSize 每页数量
     * @return 分页数据
     */
    PageInfo<Menu> page(int pageNum, int pageSize);

    /**
     * 添加菜单
     * @param menu 菜单信息
     */
    void addMenu(Menu menu);

    /**
     * 根据id删除菜单
     * @param id 菜单id
     */
    void deletedById(Long id);

    /**
     * 根据id修改菜单信息
     * @param menu 菜单信息
     */
    void updateMenuById(Menu menu);

    /**
     * 根据菜单名查询菜单数量
     * @param name 菜单名
     * @return 菜单数量
     */
    Long findCountByName(String name);
}
