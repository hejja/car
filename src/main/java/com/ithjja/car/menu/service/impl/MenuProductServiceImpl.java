package com.ithjja.car.menu.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ithjja.car.common.model.QueryPage;
import com.ithjja.car.menu.mapper.MenuProductMapper;
import com.ithjja.car.menu.model.MenuProduct;
import com.ithjja.car.menu.service.MenuProductService;

/**
 * 产品业务逻辑层接口实现
 * @author hejja
 */
@Service
public class MenuProductServiceImpl implements MenuProductService {

    /** 产品数据访问层 */
    @Autowired
    private MenuProductMapper menuProductMapper;

    @Override
    public List<MenuProduct> findLimit(Integer number) {
        return menuProductMapper.findLimit(number);
    }

    @Override
    public MenuProduct findById(Long id) {
        return menuProductMapper.findById(id);
    }

    @Override
    public Double findPriceById(Long id) {
        return menuProductMapper.findPriceById(id);
    }

    @Override
    public Long findCountByMind(Long mid) {
        return menuProductMapper.findCountByMid(mid);
    }

    @Override
    public PageInfo<MenuProduct> page(Long mid, QueryPage queryPage) {
        PageHelper.startPage(queryPage.getPage(), queryPage.getSize());
        return new PageInfo<>(menuProductMapper.page(mid));
    }

    @Override
    public PageInfo<MenuProduct> pageAll(QueryPage queryPage) {
        PageHelper.startPage(queryPage.getPage(), queryPage.getSize());
        return new PageInfo<>(menuProductMapper.pageAll());
    }

    @Override
    public List<MenuProduct> findByMid(Long mid) {
        return menuProductMapper.page(mid);
    }

    @Override
    public void add(MenuProduct menuProduct) {
        menuProduct.setCreateTime(new Date());
        menuProduct.setImgPath("/imgPath/" + menuProduct.getImgPath());
        menuProductMapper.add(menuProduct);
    }
}
