package com.ithjja.car.menu.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ithjja.car.menu.model.FrontMenu;
import com.ithjja.car.menu.model.Menu;
import com.ithjja.car.menu.service.MenuService;

/**
 * 菜单的控制层
 * @author hejja
 */
@RestController
@RequestMapping("menu")
public class MenuController {

    /** 菜单的业务逻辑层接口 */
    @Autowired
    private MenuService menuService;

    /**
     * 前台展示数据
     * @return 前台数据
     */
    @GetMapping("/frontDisplay")
    public ResponseEntity<?> frontDisplay() {
        FrontMenu frontMenu = menuService.frontDisplay();
        return ResponseEntity.ok(frontMenu);
    }

    /**
     * 查询所有菜单
     * @return 菜单集合
     */
    @GetMapping("/findAll")
    public ResponseEntity<List<Menu>> findAll() {
        return ResponseEntity.ok(menuService.findAllUnDeleted());
    }

    /**
     * 分页查询菜单
     * @param pageNum 第几页
     * @param pageSize 每页数量
     * @return 分页数据
     */
    @GetMapping("/page/{pageNum}/{pageSize}")
    public ResponseEntity<PageInfo<Menu>> page(@PathVariable("pageNum") int pageNum,
        @PathVariable("pageSize") int pageSize) {
        PageInfo<Menu> page = menuService.page(pageNum, pageSize);
        return ResponseEntity.ok(page);
    }

    /**
     * 添加菜单
     * @param name 菜单名
     * @return 响应
     */
    @PutMapping("/addMenu/{name}")
    public ResponseEntity<?> addMenu(@PathVariable("name") String name) {
        Menu menu = new Menu();
        menu.setName(name);
        menu.setCreateTime(new Date());
        menu.setSort(new Date().getTime());
        menuService.addMenu(menu);
        return ResponseEntity.noContent().build();
    }

    /**
     * 删除菜单
     * @param id 菜单id
     * @return 响应
     */
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        menuService.deletedById(id);
        return ResponseEntity.noContent().build();
    }
}
