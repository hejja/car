package com.ithjja.car.menu.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.ithjja.car.common.model.QueryPage;
import com.ithjja.car.menu.model.MenuProduct;
import com.ithjja.car.menu.service.MenuProductService;

/**
 * 产品的控制层
 * @author hejja
 */
@RestController
@RequestMapping("product")
public class ProductController {

    /** 产品业务逻辑层接口 */
    @Autowired
    private MenuProductService productService;

    /**
     * 根据产品id查询产品详情
     * @param id 产品id
     * @return 产品详情
     */
    @GetMapping("/{id}")
    public ResponseEntity<MenuProduct> detail(@PathVariable("id") Long id) {
        return ResponseEntity.ok(productService.findById(id));
    }

    /**
     * 根据菜单id分页查询产品
     * @param mid 菜单id
     * @param params 分页条件
     * @return 产品集合
     */
    @GetMapping("/page/{mid}")
    public ResponseEntity<PageInfo<MenuProduct>> page(@PathVariable("mid") Long mid, QueryPage params) {
        return ResponseEntity.ok(productService.page(mid, params));
    }

    /**
     * 分页查询产品
     * @param params 分页条件
     * @return 产品集合
     */
    @GetMapping("/page/all")
    public ResponseEntity<PageInfo<MenuProduct>> pageAll(QueryPage params) {
        return ResponseEntity.ok(productService.pageAll(params));
    }

    /**
     * 根据菜单id分页查询产品
     * @param mid 菜单id
     * @return 产品集合
     */
    @GetMapping("/findByMid/{mid}")
    public ResponseEntity<List<MenuProduct>> findByMid(@PathVariable("mid") Long mid) {
        return ResponseEntity.ok(productService.findByMid(mid));
    }

    @PutMapping("/add")
    public ResponseEntity<?> add(@RequestBody MenuProduct menuProduct) {
        productService.add(menuProduct);
        return ResponseEntity.noContent().build();
    }
}
