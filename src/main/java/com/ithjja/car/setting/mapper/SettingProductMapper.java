package com.ithjja.car.setting.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.ithjja.car.setting.model.SettingProduct;

/**
 * 产品配置数据访问层
 * @author hejja
 */
@Mapper
public interface SettingProductMapper {

    /**
     * 根据配置名查询配置信息
     * @param name 配置名
     * @return 产品配置信息
     */
    @Select("select * from tbl_setting_product where name = #{name}")
    SettingProduct findByName(@Param("name") String name);
}
