package com.ithjja.car.setting.model;

import java.io.Serializable;

/**
 * 产品篇日志实体
 * @author hejja
 */
public class SettingProduct implements Serializable {

    /** 序列化id */
    private static final long serialVersionUID = -1969417655650197601L;

    /** 主键id 自增 */
    private Long id;

    /** 配置名 */
    private String name;

    /** 配置项 */
    private String item;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}
