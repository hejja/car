package com.ithjja.car.setting.consts;

/**
 * 系统配置常量类
 * @author hejja
 */
public class SettingConsts {

    // ================= 产品配置 =================
    // ----------------- 产品配置名称 -----------------
    /** 前台展示配置 */
    public static final String SETTING_PRODUCT_NAME_FRONT_DISPLAY = "FRONT_DISPLAY";

}
