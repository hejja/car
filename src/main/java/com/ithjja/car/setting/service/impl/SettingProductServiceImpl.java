package com.ithjja.car.setting.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ithjja.car.setting.mapper.SettingProductMapper;
import com.ithjja.car.setting.model.SettingProduct;
import com.ithjja.car.setting.service.SettingProductService;

/**
 * 产品配置业务逻辑层实现类
 * @author hejja
 */
@Service
public class SettingProductServiceImpl implements SettingProductService {

    /** 产品配置数据访问层 */
    @Autowired
    private SettingProductMapper settingProductMapper;

    @Override
    public SettingProduct findByName(String name) {
        return settingProductMapper.findByName(name);
    }
}
