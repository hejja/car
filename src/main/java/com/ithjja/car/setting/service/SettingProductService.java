package com.ithjja.car.setting.service;

import com.ithjja.car.setting.model.SettingProduct;

/**
 * 产品配置业务逻辑层接口
 * @author hejja
 */
public interface SettingProductService {

    /**
     * 根据配置名查询配置信息
     * @param name 配置名
     * @return 产品配置信息
     */
    SettingProduct findByName(String name);
}
