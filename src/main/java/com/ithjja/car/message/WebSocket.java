package com.ithjja.car.message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import com.ithjja.car.common.model.Chat;

/**
 * @author hejja
 */
@Controller
public class WebSocket {

    /** 消息模板对象 */
    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/message")
    public void handleOrder(Chat chat) {
        messagingTemplate.convertAndSendToUser(chat.getTo(), "/queue/chat", chat);
    }

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public ResponseEntity<?> greeting(String message) throws Exception {
        return ResponseEntity.ok(message);
    }
}
